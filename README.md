[![Build status](https://ci.appveyor.com/api/projects/status/9tbl0avy1oh6oq1l?svg=true)](https://ci.appveyor.com/project/luisfmcalado/ci-appveyor)

# README #

Testing AppVeyor CI

# Build #

```
cmake -G"Visual Studio 12" -H. -Bbuild -DBOOST_ROOT="C:\boost_1_58_0" -DBOOST_LIBRARYDIR="C:\boost_1_58_0\stage\lib" -DBoost_USE_STATIC_LIBS="ON"
```
```
cmake --build build --config Debug
```
```
ctest -C Debug -VV
```

# Credits #

MIT © [Luis Calado](https://github.com/luisfmcalado/)